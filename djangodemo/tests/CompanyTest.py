import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.Company import Company
from djangodemo.delegates.CompanyDelegate import CompanyDelegate

 #======================================================================
# 
# Encapsulates data for model Company
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class CompanyTest Declaration
#======================================================================
class CompanyTest (TestCase) :
	def test_crud(self) :
		company = Company()
		company.name = "default name field value"
		company.type = "default type field value"
		
		delegate = CompanyDelegate()
		responseObj = delegate.create(company)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


