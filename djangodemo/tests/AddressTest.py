import datetime

from django.test import TestCase
from django.utils import timezone
from djangodemo.models.Address import Address
from djangodemo.delegates.AddressDelegate import AddressDelegate

 #======================================================================
# 
# Encapsulates data for model Address
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class AddressTest Declaration
#======================================================================
class AddressTest (TestCase) :
	def test_crud(self) :
		address = Address()
		address.street = "default street field value"
		address.city = "default city field value"
		address.state = "default state field value"
		address.zipCode = "default zipCode field value"
		
		delegate = AddressDelegate()
		responseObj = delegate.create(address)
		
		self.assertEqual(responseObj, delegate.get( responseObj.id ))
	
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 1 )		
		delegate.delete(responseObj.id)
		
		allObj = delegate.getAll()
		self.assertEqual(allObj.count(), 0 )		


