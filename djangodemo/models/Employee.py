from django.db import models

#======================================================================
# 
# Encapsulates data for model Employee
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class Employee Declaration
#======================================================================
class Employee (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	firstName = models.CharField(max_length=200, null=True)
	lastName = models.CharField(max_length=200, null=True)

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.firstName
		str = str + self.lastName
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Employee";
    
	def objectType(self):
		return "Employee";
