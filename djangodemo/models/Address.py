from django.db import models

#======================================================================
# 
# Encapsulates data for model Address
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class Address Declaration
#======================================================================
class Address (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	street = models.CharField(max_length=200, null=True)
	city = models.CharField(max_length=200, null=True)
	state = models.CharField(max_length=200, null=True)
	zipCode = models.CharField(max_length=200, null=True)

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.street
		str = str + self.city
		str = str + self.state
		str = str + self.zipCode
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Address";
    
	def objectType(self):
		return "Address";
