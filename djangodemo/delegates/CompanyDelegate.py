from django.core import exceptions
from django.core import serializers
from django.db import models
from django.db import utils

from djangodemo.models.Company import Company
from djangodemo.models.Employee import Employee
from djangodemo.models.Address import Address
from djangodemo.exceptions import Exceptions

 #======================================================================
# 
# Encapsulates data for model Company
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class CompanyDelegate Declaration
#======================================================================
class CompanyDelegate :

#======================================================================
# Function Declarations
#======================================================================

	def get(self, companyId ):
		try:	
			company = Company.objects.filter(id=companyId)
			return company.first();
		except Company.DoesNotExist:
			raise ProcessingError("Company with id " + str(companyId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 

	def createFromJson(self, company):
		for model in serializers.deserialize("json", company):
			model.save()
			return model;

	def create(self, company):
		company.save()
		return company;

	def saveFromJson(self, company):
		for model in serializers.deserialize("json", company):
			model.save()
			return company;
	
	def save(self, company):
		company.save()
		return company;
	
	def delete(self, companyId ):
		errMsg = "Failed to delete Company from db using id " + str(companyId)
		
		try:
			company = Company.objects.get(id=companyId)
			company.delete()
			return True
		except Company.DoesNotExist:
			raise ProcessingError("Company with id " + str(companyId) + " does not exist.")
		except utils.DatabaseError:
			raise StorageReadError()
		except Exception:
			raise GeneralError(errMsg) 
	
	def getAll(self):
		try:
			all = Company.objects.all()
			return all;
		except utils.DatabaseError:
			raise StorageReadError("Failed to get all Company from db")
		except Exception:
			return None;
		
	def assignAddress( self, companyId, addressId ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.AddressDelegate import AddressDelegate

		errMsg = "Failed to assign element " + str(addressId) + " for Address on Company"

		try:
			# get the Company from db
			company = self.get( companyId ).first()	
			
			# get the Address from db
			address = AddressDelegate().get(addressId).first();
			
			# assign the Address		
			company.address = address
			
			#save it
			company.save()

			# reload and return the appropriate version					
			return self.get( companyId );
		except Company.DoesNotExist:
			raise ProcessingError(errMsg + " : Company with id " + str(companyId) + " does not exist.")
		except Address.DoesNotExist:
			raise ProcessingError(errMsg + " : Address with id " + str(addressId) + " does not exist.")
		except Exception:
			return None;
				
	def unassignAddress( self, companyId ):
		errMsg = "Failed to unassign element " + str(addressId) + " for Address on Company"

		try:
			# get the Company from db
			company = self.get( companyId ).first()	
			
			# assign to None for unassignment
			company.address = None			

			#save it
			company.save()

			# reload and return the appropriate version					
			return self.get( companyId );
		except Company.DoesNotExist:
			raise ProcessingError(errMsg + " : Company with id " + str(companyId) + " does not exist.")
		except Exception:
			return None;
		
	def addEmployees( self, companyId, employeesIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.EmployeeDelegate import EmployeeDelegate

		errMsg = "Failed to add elements " + str(employeesIds) + " for Employees on Company"

		try:
			# get the Company
			company = self.get( companyId ).first()
				
			# split on a comma with no spaces
			idList = employeesIds.split(',')

			
			# iterate over ids
			for id in idList:
				# read the Employee		
				employee = EmployeeDelegate().get(id).first();	
				# add the Employee
				company.employees.add(employee)
				
			# save it		
			company.save()
			
			# reload and return the appropriate version
			return self.get( companyId );
		except Company.DoesNotExist:
			raise ProcessingError(errMsg + " : Company with id " + str(companyId) + " does not exist.")
		except Employee.DoesNotExist:
			raise ProcessingError(errMsg + " : Employee does not exist.")
		except Exception:
			raise ProcessingError(errMsg) 
		
	def removeEmployees( self, companyId, employeesIds ):
		# lazy importing avoids circular dependencies
		from djangodemo.delegates.EmployeeDelegate import EmployeeDelegate

		errMsg = "Failed to remove elements " + str(employeesIds) + " for Employees on Company"

		try:
			# get the Company
			company = self.get( companyId ).first()
				
			# split on a comma with no spaces
			idList = employeesIds.split(',')
			
			# iterate over ids
			for id in idList:
				# read the Employee		
				employee = EmployeeDelegate().get(id).first();	
				# add the Employee
				company.employees.remove(employee)
				
			# save it		
			company.save()
			
			# reload and return the appropriate version
			return self.get( companyId );
		except Company.DoesNotExist:
			raise ProcessingError(errMsg + " : Company with id " + str(companyId) + " does not exist.")
		except Employee.DoesNotExist:
			raise ProcessingError(errMsg + " : Employee does not exist.")
		except utils.DatabaseError:
			raise StorageWriteError()
		except Exception:
			raise GeneralError(errMsg) 
		
