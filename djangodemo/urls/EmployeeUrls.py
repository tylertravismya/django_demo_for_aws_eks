from django.urls import path
from djangodemo.views import EmployeeView

urlpatterns = [
    path('', EmployeeView.index, name='index'),
	path('create', EmployeeView.get, name='create'),
	path('get/<int:employeeId>/', EmployeeView.get, name='get'),
	path('save', EmployeeView.save, name='save'),
	path('getAll', EmployeeView.getAll, name='getAll'),
	path('delete/<int:employeeId>/', EmployeeView.delete, name='delete'),
]
