# Variables
variable "aws-access-key" {}
variable "aws-secret-key" {}
variable "region" {}

# Modules
module "eks" {
  source   = "./eks"
  region   = "${var.region}"
  aws-access-key = "${var.aws-access-key}"
  aws-secret-key = "${var.aws-secret-key}"
}

module "k8s" {
  source   = "./k8s"
}