# AWS Cluster
data "aws_eks_cluster" "djangodemo-cluster" {
  name = "djangodemo-cluster"
}

output "endpoint" {
  value = "${data.aws_eks_cluster.djangodemo-cluster.endpoint}"
}

output "kubeconfig-certificate-authority-data" {
  value = "${data.aws_eks_cluster.djangodemo-cluster.certificate_authority.0.data}"
}